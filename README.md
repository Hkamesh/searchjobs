### Solution of https://gitlab.com/byjusacs/fsdevchallenge

### prerequisites

the Application is made using laravel and react, you need have following install on you system

 1.	php(version used 7.3.2)
 2.	laravel(version used 5.7.26)
 3.	Node(version used 11.10.0)


### Follow the steps below to start the application

To start application please run 
`php artisan serve` and
`npm run watch`

now you should be able to access the application at 'http://localhost:8000'

### Usages

1. visit http://localhost:8000
2. To list all job listing click on `Show all jobs` button.
3. To search jobs, select criteria from the dropdown, type value to be search and  select the sorting type click on `Search`
	e.g you can select title from dropdwon, type dfx and click on search

### To Directly visit the endpoints
	http://localhost:8000/jobs/{searchType}?sort_by={sortByValue}&sort={sortBy}&query={query}

	valid options for:
		searchType: [jobtitle,companyname,location,skills,source,salary,type,experience,startdate,enddate]
		sortByValue: [location,type]
		sortBy: [asc,desc]
 
 Examples:

- All Jobs => http://localhost:8000/jobs
- jobtitle => http://localhost:8000/jobs/title?query=dfx
- companyname => http://localhost:8000/jobs/companyname?query=Onco
- location [CITY] => http://localhost:8000/jobs/location?query=Bengaluru
- skills [C,Nodejs] => http://localhost:8000/jobs/skills?query=css
- source => http://localhost:8000/jobs/source?query=techgig
- salary => http://localhost:8000/jobs/salary?query=8000
- type => http://localhost:8000/jobs/type?query=intern
- type sort by location => http://localhost:8000/jobs/type?query=intern&sort_by=location
- type sort by type => http://localhost:8000/jobs/type?query=intern&sort_by=type
- experience => http://localhost:8000/jobs/experience?query=8
- startdate => http://localhost:8000/jobs/startdate?query=2018-07-14
- enddate => http://localhost:8000/jobs/enddate?query=2019-02-02

for sorting sorting by:

- location => http://localhost:8000/jobs/type?query=intern&sort_by=location
- type => http://localhost:8000/jobs/type?query=intern&sort_by=type

to run unit tests, please go to project base directory and run: 
`vendor/bin/phpunit`
